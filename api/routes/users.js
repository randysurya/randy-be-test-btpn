const express = require('express');
const router = express.Router();
const bcrypt = require("bcrypt");
const mongoose = require('mongoose');

const User = require('../models/user');
const UserController = require('../controllers/user');

//get all users data
router.get('/personal/:number', UserController.get_users);

//logout
router.get('/:userId/logout', UserController.user_logout);

//delete user
router.delete('/:email', UserController.user_delete);

//update 
router.put('/:username', UserController.user_update);

module.exports = router;
