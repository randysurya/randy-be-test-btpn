const express = require('express');
const router = express.Router();
const bcrypt = require("bcrypt");
const mongoose = require('mongoose');

const User = require('../models/user');
const UserController = require('../controllers/user');

//register new user
router.post('/signup', UserController.user_signup);

//login
router.post('/login', UserController.user_login);

module.exports = router;
