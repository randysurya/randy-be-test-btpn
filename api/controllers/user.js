const express = require('express');
const bcrypt = require("bcrypt");
const mongoose = require('mongoose');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

const User = require("../models/user");

exports.user_signup = (req, res, next) => {

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  } else {
    User.find({ email: req.body.email_address })
      .then(user => {
        if (user.length >= 1) {
          return res.status(409).json({ message: "Account with this email already registered" });
        } else {
          bcrypt.hash(req.body.password, 10, (err, hash) => {
            if (err) {
              return res.status(500).json({ error: err });
            } else {
              const user = new User({
                _id: new mongoose.Types.ObjectId(),
                userName: req.body.username,
                accountNumber: req.body.account_number,
                emailAddress: req.body.email_address,
                identityNumber: req.body.identity_number,
                password: hash,
              });
              user.save()
                .then(user => {
                  res.status(201).json({ message: "User created" });
                })
                .catch(err => {
                  console.log(err);
                  res.status(500).json({ error: err });
                });
            }
          });
        }
      });
  }
}

exports.user_login = (req, res, next) => {
  User.findOne({ email: req.body.email }).exec().then(user => {
    if (!user) {
      return res.status(401).json({ message: 'Auth failed' });
    }
    bcrypt.compare(req.body.password, user.password, (err, result) => {
      if (err) {
        return res.status(401).json({ message: 'Auth failed' });
      }
      if (result) {
        const token = jwt.sign({ email_address: user.emailAddress, password: user.password }, process.env.JWT_SECRET, {
          expiresIn: '1h'
        })
        return res.status(200).json({ message: 'Auth successful', token });
      }
      res.status(401).json({ message: 'Auth failed' });
    });
  }).catch(err => {
    console.log(err);
    res.status(500).json({ error: err });
  });
};

exports.user_delete = (req, res, next) => {
  User.deleteOne({ emailAddress: req.params.email }).exec().then(result => {
    res.status(200).json({ message: 'User deleted' });
  }).catch(err => {
    console.log(err);
    res.status(500).json({ error: err });
  });
};

exports.user_update = (req, res, next) => {
  User.updateOne({
    userName: req.params.username, $set: {
      emailAddress: req.body.email_address,
      identityNumber: req.body.identity_number,
      accountNumber: req.body.account_number,
    }
  }).exec().then(result => {
    res.status(200).json({ message: 'User updated' });
  }).catch(err => {
    console.log(err);
    res.status(500).json({ error: err });
  });
};

exports.get_users = (req, res, next) => {
  User.findOne({ $or: [{ identityNumber: req.params.number }, { accountNumber: req.params.number }] }).exec().then(result => {
    res.status(200).json({ result });
  }).catch(err => {
    console.log(err);
    res.status(500).json({ error: err });
  });
};

exports.get_all_users = (req, res, next) => {
  User.find().exec().then(result => {
    res.status(200).json({ result });
  }).catch(err => {
    console.log(err);
    res.status(500).json({ error: err });
  });
};

exports.user_logout = (req, res, next) => {
  if (req.session) {
    req.session.destroy(function (err) {
      if (err) {
        res.status(500).json({ error: err });
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }
};
